#include "number.h"

#include <iostream>
#include <cmath>

int main(void)
{
    for(int i = 0; i < 5000; i += 100)
    {
        std::cout << i << " : " << Number::calcPI(i) << std::endl;
    }

    //auto a = Number("0.010001666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666");
    //auto b = Number("-0.00000000000008333333333333333333333333333333333333333333333333333333333333333333333333333333333333333");

    //std::cout << a  << " + " << b << " = " << a + b << std::endl;

    //std::cout << Number("0.0064") * Number("1") << std::endl;

    //std::cout << Number("0.01") * Number("0.01") * Number("0.01") << std::endl;

    /*number_t adt;

    adt = "-123";
    std::cout << adt.toString() << std::endl;

    adt = "-1234564685465156316541613546854984984351321632163546548949496545324131321313216496854984948651.45646465465461321321366464";
    std::cout << adt.toString() << std::endl;

    adt = "-12345646854651563165416121313216496854984948651.000000000000000000000045646465465461321321366464";
    std::cout << adt.toString() << std::endl;

    number_t adt2;

    adt = "123";
    adt2 = "123";

    if(adt == adt2)
        std::cout << "== works" << std::endl;

    number_t adt3;
    adt3 = "122";

    if(adt2 > adt3)
        std::cout << "> works" << std::endl;

    if(adt3 < adt2)
        std::cout << "< works" << std::endl;

    if(adt >= adt2 && adt >= adt3)
        std::cout << ">= works" << std::endl;

    if(adt <= adt2 && adt3 <= adt)
        std::cout << "<= works" << std::endl;

    std::cout << adt << " + " << adt2 << " = " << adt + adt2 << std::endl;
    std::cout << adt << " + " << adt3 << " = " << adt + adt3 << std::endl;
    std::cout << adt2 << " + " << adt << " = " << adt2 + adt << std::endl;
    std::cout << adt3 << " + " << adt << " = " << adt3 + adt << std::endl;

    std::cout << adt << " - " << adt2 << " = " << adt - adt2 << std::endl;
    std::cout << adt << " - " << adt3 << " = " << adt - adt3 << std::endl;
    std::cout << adt2 << " - " << adt << " = " << adt2 - adt << std::endl;
    std::cout << adt3 << " - " << adt << " = " << adt3 - adt << std::endl;

    number_t adt4;
    adt4 = "-10";

    std::cout << adt << " + " << adt4 << " = " << adt + adt4 << std::endl;
    std::cout << adt4 << " + " << adt << " = " << adt4 + adt << std::endl;

    std::cout << adt << " - " << adt4 << " = " << adt - adt4 << std::endl;
    std::cout << adt4 << " - " << adt << " = " << adt4 - adt << std::endl;

    *//*number_t adt5, adt6;
    adt5 = "10.6";
    adt6 = "-10.5";
    Number adt; adt = "5.5";
    Number adt2; adt2 = "6.3";

    std::cout << adt5 << " + " << adt6 << " = " << adt5 + adt6 << std::endl;
    std::cout << adt6 << " + " << adt5 << " = " << adt6 + adt5 << std::endl;
    std::cout << adt << " + " << adt5 << " = " << adt + adt5 << std::endl;
    std::cout << adt << " + " << adt6 << " = " << adt + adt6 << std::endl;
    std::cout << adt << " + " << adt2 << " = " << adt + adt2 << std::endl;
    std::cout << adt5 << " + " << adt << " = " << adt5 + adt << std::endl;
    std::cout << adt6 << " + " << adt << " = " << adt6 + adt << std::endl;
    std::cout << adt2 << " + " << adt << " = " << adt2 + adt << std::endl;

    std::cout << adt5 << " - " << adt6 << " = " << adt5 - adt6 << std::endl;
    std::cout << adt6 << " - " << adt5 << " = " << adt6 - adt5 << std::endl;
    std::cout << adt << " - " << adt5 << " = " << adt - adt5 << std::endl;
    std::cout << adt << " - " << adt6 << " = " << adt - adt6 << std::endl;
    std::cout << adt << " - " << adt2 << " = " << adt - adt2 << std::endl;
    std::cout << adt5 << " - " << adt << " = " << adt5 - adt << std::endl;
    std::cout << adt6 << " - " << adt << " = " << adt6 - adt << std::endl;
    std::cout << adt2 << " - " << adt << " = " << adt2 - adt << std::endl;

    *//*number_t adt7, adt8, adt9;
    adt7 = "1";
    adt8 = "0";
    adt9 = "-1";
    adt3 = "10";

    std::cout << adt4 << " * " << adt7 << " = " << adt4 * adt7 << std::endl;
    std::cout << adt4 << " * " << adt8 << " = " << adt4 * adt8 << std::endl;
    std::cout << adt4 << " * " << adt9 << " = " << adt4 * adt9 << std::endl;
    std::cout << adt3 << " * " << adt7 << " = " << adt3 * adt7 << std::endl;
    std::cout << adt3 << " * " << adt8 << " = " << adt3 * adt8 << std::endl;
    std::cout << adt3 << " * " << adt9 << " = " << adt3 * adt9 << std::endl;
    std::cout << adt7 << " * " << adt4 << " = " << adt7 * adt4 << std::endl;
    std::cout << adt8 << " * " << adt4 << " = " << adt8 * adt4 << std::endl;
    std::cout << adt9 << " * " << adt4 << " = " << adt9 * adt4 << std::endl;
    std::cout << adt7 << " * " << adt3 << " = " << adt7 * adt3 << std::endl;
    std::cout << adt8 << " * " << adt3 << " = " << adt8 * adt3 << std::endl;
    std::cout << adt9 << " * " << adt3 << " = " << adt9 * adt3 << std::endl;

    adt4 = "125.123";
    adt3 = "-125.123";

    std::cout << adt4 << " * " << adt5 << " = " << adt4 * adt5 << std::endl;
    std::cout << adt4 << " * " << adt6 << " = " << adt4 * adt6 << std::endl;
    std::cout << adt3 << " * " << adt5 << " = " << adt3 * adt5 << std::endl;
    std::cout << adt3 << " * " << adt6 << " = " << adt3 * adt6 << std::endl;
    std::cout << adt5 << " * " << adt4 << " = " << adt5 * adt4 << std::endl;
    std::cout << adt6 << " * " << adt4 << " = " << adt6 * adt4 << std::endl;
    std::cout << adt5 << " * " << adt3 << " = " << adt5 * adt3 << std::endl;
    std::cout << adt6 << " * " << adt3 << " = " << adt6 * adt3 << std::endl;

    adt = "100";
    adt2 = "2.5";
    adt3 = "5";
    adt4 = "2.54";
    adt5 = "3";

    std::cout << adt << " / " << adt2 << " = " << adt / adt2 << std::endl;
    std::cout << adt << " / " << adt3 << " = " << adt / adt3 << std::endl;
    std::cout << adt << " / " << adt4 << " = " << adt / adt4 << std::endl;
    std::cout << adt << " / " << adt5 << " = " << adt / adt5 << std::endl;*/

    return 0;
}