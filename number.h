#ifndef NUMBER_H
#define NUMBER_H

#include <cstdint>
#include <vector>
#include <string>

class Exception
    : public std::exception
{
    std::string message;

    public:

        Exception(std::string const& message);

        const char* what() const noexcept override;

        std::string const& getMessage() const;
};

class InvalidNumberException
    : public Exception
{

    public:

        InvalidNumberException();

};

class OverflowException
    : public Exception
{
    OverflowException(std::string const& msg);

    public:

        template <typename T>
        static OverflowException create(T max, T pushed);

};

class UnsignedWholeNumber
{
    protected:
        size_t length = 1;
        std::vector<uint64_t> numbers;

    public:

        UnsignedWholeNumber();
        UnsignedWholeNumber(std::string const& number);
        ~UnsignedWholeNumber();

        static bool isStringInstanceValid(std::string const& number);

        UnsignedWholeNumber& operator=(std::string const& num);
        UnsignedWholeNumber& operator=(UnsignedWholeNumber const& num);
        
        UnsignedWholeNumber operator+(UnsignedWholeNumber const& num) const;
        UnsignedWholeNumber operator-(UnsignedWholeNumber const& num) const;
        UnsignedWholeNumber operator*(UnsignedWholeNumber const& num) const;
        UnsignedWholeNumber operator/(UnsignedWholeNumber const& num) const;
        UnsignedWholeNumber operator%(UnsignedWholeNumber const& num) const;

        UnsignedWholeNumber& operator+=(UnsignedWholeNumber const& num);
        UnsignedWholeNumber& operator-=(UnsignedWholeNumber const& num);
        UnsignedWholeNumber& operator*=(UnsignedWholeNumber const& num);
        UnsignedWholeNumber& operator/=(UnsignedWholeNumber const& num);
        UnsignedWholeNumber& operator++();
        UnsignedWholeNumber& operator--();
        UnsignedWholeNumber& operator++(int);
        UnsignedWholeNumber& operator--(int);

        bool operator==(UnsignedWholeNumber const& num) const;
        bool operator!=(UnsignedWholeNumber const& num) const;
        bool operator>(UnsignedWholeNumber const& num) const;
        bool operator<(UnsignedWholeNumber const& num) const;
        bool operator>=(UnsignedWholeNumber const& num) const;
        bool operator<=(UnsignedWholeNumber const& num) const;

        friend std::ostream& operator<<(std::ostream& os, UnsignedWholeNumber const& num);

        virtual void setDigitAtOffset(size_t offset, uint8_t num);
        virtual uint8_t getDigitAtOffset(size_t offset) const;

        virtual std::string toString() const;
        bool isZero() const;
        size_t getLength() const;

    private:

        //Frees up space if is possible.
        void normalize();
};

class UnsignedWholeNumberZero
    : public UnsignedWholeNumber
{
        size_t startZeros = 0;

        void updateZeroIndex();

    public:
        UnsignedWholeNumberZero();
        UnsignedWholeNumberZero(std::string const& number);

        UnsignedWholeNumberZero& operator=(std::string const& num);
        UnsignedWholeNumberZero& operator=(UnsignedWholeNumberZero const& num);

        UnsignedWholeNumberZero operator+(UnsignedWholeNumberZero const& num) const;
        UnsignedWholeNumberZero operator-(UnsignedWholeNumberZero const& num) const;
        UnsignedWholeNumberZero operator*(UnsignedWholeNumberZero const& num) const;
        UnsignedWholeNumberZero operator/(UnsignedWholeNumberZero const& num) const;
        UnsignedWholeNumberZero operator%(UnsignedWholeNumberZero const& num) const;

        UnsignedWholeNumberZero& operator+=(UnsignedWholeNumberZero const& num);
        UnsignedWholeNumberZero& operator-=(UnsignedWholeNumberZero const& num);
        UnsignedWholeNumberZero& operator*=(UnsignedWholeNumberZero const& num);
        UnsignedWholeNumberZero& operator/=(UnsignedWholeNumberZero const& num);

        bool operator==(UnsignedWholeNumberZero const& num) const;
        bool operator!=(UnsignedWholeNumberZero const& num) const;
        bool operator>(UnsignedWholeNumberZero const& num) const;
        bool operator<(UnsignedWholeNumberZero const& num) const;
        bool operator>=(UnsignedWholeNumberZero const& num) const;
        bool operator<=(UnsignedWholeNumberZero const& num) const;

        std::string toString() const override;

        void removeLastDigit();
        void removeFirstDigit();

        void setDigitAtOffset(size_t offset, uint8_t num) override;
        uint8_t getDigitAtOffset(size_t offset) const override;

};

/**
 * Should not be used.
 * Isn't implemented fully!
 **/
class WholeNumber
    : public UnsignedWholeNumber
{

    public:

        static bool isStringInstanceValid(std::string const& number);


};

class Number {
    static size_t dividePrecision;

    bool positive;
    UnsignedWholeNumber integer;
    UnsignedWholeNumberZero floatingPoint;

    /**
     * Converts 15,20 => 1520
     * Always is positive.
     **/
    UnsignedWholeNumber convertToWholeNumber() const;

    public:

        static Number calcFactorial(int n);
        static Number findNextPrime(Number begin);
        static Number calcPower(double d, int p);
        static Number calcSin(double d);
        static Number calcCos(double d);
        static Number calcSqrt(double d);
        static Number calcSqrt(Number const& num);
        static Number calcLogN(double d);
        static Number calcLogN(Number const& num);
        static Number calcPI(int digits);
        static Number calcAverage(Number* data, int n);
        static int find(Number* data, int n, Number const& num);
        static void sort(Number* data, int n);
        static Number const& getSmallestNumber();

        static bool isStringInstanceValid(std::string const& number);
        static size_t getDividePrecision() { return Number::dividePrecision; }
        static void setDividePrecision(size_t a) { Number::dividePrecision = a; }

        Number();
        Number(std::string const& num);
        explicit Number(double n);
        ~Number();

        Number calcPower(int pow) const;

        Number const& operator=(std::string const& num);

        Number operator+(std::string const& num) const;
        Number operator+(Number const& num) const;
        Number operator-(std::string const& num) const;
        Number operator-(Number const& num) const;
        Number operator*(std::string const& num) const;
        Number operator*(Number const& num) const;
        Number operator/(std::string const& num) const;
        Number operator/(Number const& num) const;
        Number operator%(std::string const& num) const;
        Number operator%(Number const& num) const;

        Number& operator+=(std::string const& num);
        Number& operator+=(Number const& num);
        Number& operator-=(std::string const& num);
        Number& operator-=(Number const& num);
        Number& operator*=(std::string const& num);
        Number& operator*=(Number const& num);
        Number& operator/=(std::string const& num);
        Number& operator/=(Number const& num);
        Number& operator++();
        Number& operator--();
        Number& operator++(int);
        Number& operator--(int);

        bool operator==(Number const& num) const;
        bool operator!=(Number const& num) const;
        bool operator>(Number const& num) const;
        bool operator<(Number const& num) const;
        bool operator>=(Number const& num) const;
        bool operator<=(Number const& num) const;

        friend std::ostream& operator<<(std::ostream& os, Number const& num);

        Number absolute() const; // Returns always positive.
        Number inverse() const; // Converts 0.25 => 4

        UnsignedWholeNumber const& getInteger() const;
        UnsignedWholeNumberZero const& getFloatingPoint() const;
        std::string toString() const;
        bool isWhole() const;
        bool isPositive() const;
        bool isNegative() const;
        bool isZero() const;
        bool isPrime() const;
};

typedef Number number_t;

#endif