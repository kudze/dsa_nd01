#include "number.h"

#include <sstream>
#include <regex>

#define MAX_UINT64_LENGTH 19

#include <iostream>

using namespace std;

uint64_t max(uint64_t a, uint64_t b)
{
    return a > b ? a : b;
}

uint64_t min(uint64_t a, uint64_t b)
{
    return a > b ? b : a;
}

uint64_t pow(uint64_t number, uint8_t power)
{
    if(power == 0)
        return 1;

    uint64_t result = number;

    for(uint8_t i = 0; i < power - 1; i++)
        result *= number;

    return result;
}

uint8_t getDigitOfNumber(uint64_t number, uint8_t offset)
{
    return (number / pow(10, offset)) % 10;
}

void setDigitOfNumber(uint64_t& number, uint8_t offset, uint8_t digit)
{
    uint8_t oldDigit = getDigitOfNumber(number, offset);

    if(oldDigit == digit)
        return;

    if(digit > oldDigit)
        number += pow(10, offset) * (digit - oldDigit);

    else
        number -= pow(10, offset) * (oldDigit - digit);
}

uint8_t convertCharToNumber(char number)
{
    return (uint8_t) (number - '0');
}

std::vector<std::string> splitString(std::string const& string, char delim)
{
    std::vector<std::string> result;

    auto lastIT = string.begin();
    for(auto it = string.begin(); it != string.end(); it++) {
        if(it == (string.end() - 1))
            result.push_back(
                std::string(lastIT, it + 1)
            );

        else if(*it == delim) {
            result.push_back(
                std::string(lastIT, it)
            );

            lastIT = it + 1;
        }
    }

    return result;
}

Exception::Exception(std::string const& message)
    : message(message)
{

}

const char* Exception::what() const noexcept
{
    return this->message.c_str();
};

std::string const& Exception::getMessage() const
{
    return this->message;
}

InvalidNumberException::InvalidNumberException()
    : Exception("Invalid Number Format")
{

}

OverflowException::OverflowException(std::string const& msg)
    : Exception(msg)
{

}

template <typename T>
OverflowException OverflowException::create(T max, T pushed)
{
    std::stringstream sstream;

    sstream << "Overflow exception, max allowed value is "
            << max << ", " << pushed << " was pushed!";

    return OverflowException(
        sstream.str()
    );
}

template <>
OverflowException OverflowException::create<uint8_t>(uint8_t max, uint8_t pushed)
{
    return OverflowException::create<int>((int) max, (int) pushed);
}

UnsignedWholeNumber::UnsignedWholeNumber()
    : UnsignedWholeNumber("0")
{

}

UnsignedWholeNumber::UnsignedWholeNumber(std::string const& num)
{
    this->operator=(num);
}

UnsignedWholeNumber::~UnsignedWholeNumber()
{

}

bool UnsignedWholeNumber::isStringInstanceValid(std::string const& number)
{
    return std::regex_match(number, std::regex("[0-9]{1,}"));
}

UnsignedWholeNumber& UnsignedWholeNumber::operator=(std::string const& num)
{
    this->length = 0;
    this->numbers.clear();

    for(auto it = num.begin(); it != num.end(); it++) {
        this->setDigitAtOffset(
            std::distance(it, num.end() - 1),
            convertCharToNumber(*it)
        );
    }

    return (*this);
}

UnsignedWholeNumber& UnsignedWholeNumber::operator=(UnsignedWholeNumber const& num)
{
    this->length = num.getLength();
    this->numbers.clear();

    this->numbers.assign(
        num.numbers.begin(),
        num.numbers.end()
    );

    return (*this);
}

UnsignedWholeNumber UnsignedWholeNumber::operator+(UnsignedWholeNumber const& num) const
{
    UnsignedWholeNumber result;

    auto tlength = this->getLength();
    auto olength = num.getLength();
    auto mlength = max(tlength, olength);

    bool reminder = false;
    for(size_t i = 0; i < mlength; i++) {
        uint8_t tdigit = this->getDigitAtOffset(i);
        uint8_t odigit = num.getDigitAtOffset(i);

        uint8_t rdigit = tdigit + odigit;

        if(reminder)
            rdigit++;

        if(rdigit > 9) {
            reminder = true;
            rdigit -= 10;
        } else reminder = false;

        result.setDigitAtOffset(i, rdigit);
    }

    if(reminder)
        result.setDigitAtOffset(mlength, 1);

    return result;
}

UnsignedWholeNumber UnsignedWholeNumber::operator-(UnsignedWholeNumber const& num) const
{
    if(num > *this)
        throw OverflowException::create<UnsignedWholeNumber>(*this, num);

    UnsignedWholeNumber result;

    auto tlength = this->getLength();
    auto olength = num.getLength();

    bool reminder = false;
    for(size_t i = 0; i < tlength; i++) {
        uint8_t tdigit = this->getDigitAtOffset(i);
        uint8_t odigit = num.getDigitAtOffset(i);

        if(reminder) {
            tdigit--;
        }

        if(tdigit == 255) {
            tdigit = 9;
            reminder = true;
        } else if(odigit > tdigit) {
            reminder = true;
            tdigit += 10;
        } else reminder = false;

        uint8_t rdigit = tdigit - odigit;

        if(rdigit != 0)
            result.setDigitAtOffset(i, rdigit);
    }

    return result;
}

UnsignedWholeNumber UnsignedWholeNumber::operator*(UnsignedWholeNumber const& num) const
{
    if(num.isZero() || this->isZero())
        return UnsignedWholeNumber("0");

    size_t tlen = this->getLength();
    size_t olen = num.getLength();

    std::vector<int> result(tlen + olen, 0);
    size_t i_n1 = 0, i_n2 = 0;
    for(size_t i = 0; i < tlen; i++) {
        size_t carry = 0;

        uint8_t tdigit = this->getDigitAtOffset(i);

        i_n2 = 0;

        for(size_t j = 0; j < olen; j++) {
            uint8_t odigit = num.getDigitAtOffset(j);

            uint64_t sum = tdigit * odigit + result[i_n1 + i_n2] + carry;

            carry = sum / 10;

            result[i_n1 + i_n2] = sum % 10;

            i_n2++;
        }

        if(carry > 0)
            result[i_n1 + i_n2] += carry;

        i_n1++;
    }

    int i = result.size() - 1;
    while(i >= 0 && result[i] == 0)
        i--;

    if(i == -1)
        return UnsignedWholeNumber("0");

    std::string s = "";

    while(i >= 0)
        s += std::to_string(result[i--]);

    return UnsignedWholeNumber(s);
}

UnsignedWholeNumber UnsignedWholeNumber::operator/(UnsignedWholeNumber const& num) const
{
    if(num.isZero())
        throw std::runtime_error("UnsignedWholeNumber is not divisible by 0");


    UnsignedWholeNumber result, t;
    result = "0";
    t = (*this);

    while(t >= num) {
        t -= num;
        result++;
    }

    return result;

    //Temp solution
    /*size_t result, t, o;
    t = std::strtoull(this->toString().c_str(), NULL, 10);
    o = std::strtoull(num.toString().c_str(), NULL, 10);

    result = t / o;

    return UnsignedWholeNumber(std::to_string(result));*/
}

UnsignedWholeNumber UnsignedWholeNumber::operator%(UnsignedWholeNumber const& num) const
{
    UnsignedWholeNumber result, t;
    t = (*this);

    while(t >= num)
        t -= num;

    return t;

    //Temp solution
    /*size_t result, t, o;
    t = std::strtoull(this->toString().c_str(), NULL, 10);
    o = std::strtoull(num.toString().c_str(), NULL, 10);

    result = t % o;

    return UnsignedWholeNumber(std::to_string(result));*/
}

UnsignedWholeNumber& UnsignedWholeNumber::operator+=(UnsignedWholeNumber const& num)
{
    (*this) = (*this) + num;

    return (*this);
}

UnsignedWholeNumber& UnsignedWholeNumber::operator-=(UnsignedWholeNumber const& num)
{
    (*this) = (*this) - num;

    return (*this);
}

UnsignedWholeNumber& UnsignedWholeNumber::operator*=(UnsignedWholeNumber const& num)
{
    (*this) = (*this) * num;

    return (*this);
}

UnsignedWholeNumber& UnsignedWholeNumber::operator/=(UnsignedWholeNumber const& num)
{
    (*this) = (*this) / num;

    return (*this);
}

UnsignedWholeNumber& UnsignedWholeNumber::operator++()
{
    UnsignedWholeNumber add;
    add = "1";

    (*this) += add;

    return (*this);
}

UnsignedWholeNumber& UnsignedWholeNumber::operator--()
{
    UnsignedWholeNumber min;
    min = "1";

    (*this) -= min;

    return (*this);
}

UnsignedWholeNumber& UnsignedWholeNumber::operator++(int)
{
    UnsignedWholeNumber add;
    add = "1";

    (*this) += add;

    return (*this);
}

UnsignedWholeNumber& UnsignedWholeNumber::operator--(int)
{
    UnsignedWholeNumber min;
    min = "1";

    (*this) -= min;

    return (*this);
}

bool UnsignedWholeNumber::operator==(UnsignedWholeNumber const& s) const
{
    return this->toString() == s.toString();
}

bool UnsignedWholeNumber::operator!=(UnsignedWholeNumber const& s) const
{
    return !this->operator==(s);
}

bool UnsignedWholeNumber::operator>(UnsignedWholeNumber const& num) const
{
    auto tlen = this->getLength();
    auto olen = num.getLength();

    if(tlen > olen)
        return true;

    if(tlen < olen)
        return false;

    for(size_t i = tlen; i-- > 0;) {
        auto tdigit = this->getDigitAtOffset(i);
        auto odigit = num.getDigitAtOffset(i);

        if(tdigit > odigit)
            return true;

        if(tdigit < odigit)
            return false;
    }

    return false;
}

bool UnsignedWholeNumber::operator<(UnsignedWholeNumber const& num) const
{
    auto tlen = this->getLength();
    auto olen = num.getLength();

    if(tlen > olen)
        return false;

    if(tlen < olen)
        return true;

    for(size_t i = tlen; i-- > 0;) {
        auto tdigit = this->getDigitAtOffset(i);
        auto odigit = num.getDigitAtOffset(i);

        if(tdigit > odigit)
            return false;

        if(tdigit < odigit)
            return true;
    }

    return false;
}

bool UnsignedWholeNumber::operator>=(UnsignedWholeNumber const& num) const
{
    return !this->operator<(num);
}

bool UnsignedWholeNumber::operator<=(UnsignedWholeNumber const& num) const
{
    return !this->operator>(num);
}

std::ostream& operator<<(std::ostream& os, UnsignedWholeNumber const& num)
{
    os << num.toString();

    return os;
}

void UnsignedWholeNumber::setDigitAtOffset(size_t offset, uint8_t digit)
{
    if(offset + 1 > this->length)
        this->length = offset + 1;

    if(digit > 10)
        throw OverflowException::create<uint8_t>(static_cast<uint8_t>(9), digit);

    size_t numberOffset = offset % MAX_UINT64_LENGTH;
    size_t index = offset / MAX_UINT64_LENGTH;

    if(this->numbers.size() <= index)
        this->numbers.resize(index + 1);

    uint64_t& number = this->numbers.at(index);

    setDigitOfNumber(number, numberOffset, digit);
}

uint8_t UnsignedWholeNumber::getDigitAtOffset(size_t offset) const
{
    if(offset >= this->getLength())
        return 0;

    size_t numberOffset = offset % MAX_UINT64_LENGTH;
    size_t index = offset / MAX_UINT64_LENGTH;

    if(this->numbers.size() <= index)
        return 0;

    uint64_t number = this->numbers.at(index);

    return getDigitOfNumber(number, numberOffset);
}

std::string UnsignedWholeNumber::toString() const
{
    std::stringstream sstream;

    if(this->getLength() == 0)
        sstream << 0;

    else {
        for(size_t i = this->getLength(); i-- > 0;)
            sstream << (int) this->getDigitAtOffset(i);
    }

    return sstream.str();
}

bool UnsignedWholeNumber::isZero() const
{
    return (
               this->numbers.size() == 0
           ) || (
               this->numbers.at(0) == 0
               && this->numbers.size() == 1
           );
}

size_t UnsignedWholeNumber::getLength() const
{
    /*
    size_t mlength = this->numbers.size() * MAX_UINT64_LENGTH;
        
    for(auto i = mlength; i --> 0;)
    {
        auto digit = this->getDigitAtOffset(i);

        if(digit != 0)
            return i + 1 + this->startZeros;
    }

    return this->startZeros;*/

    if(this->isZero())
        return 0;

    return this->length;
}

UnsignedWholeNumberZero::UnsignedWholeNumberZero()
    : UnsignedWholeNumber()
{

}

UnsignedWholeNumberZero::UnsignedWholeNumberZero(std::string const& num)
    : UnsignedWholeNumber()
{
    this->operator=(num);
}

void UnsignedWholeNumberZero::updateZeroIndex()
{
    this->startZeros = 0;
    for(int i = this->length; i-- > 0;) {
        if(this->getDigitAtOffset(i) == 0)
            this->startZeros++;

        else break;
    }

    if(this->startZeros == this->length)
        this->startZeros = 0;
}

void UnsignedWholeNumberZero::removeLastDigit()
{
    if(this->length == 0)
        return;

    this->setDigitAtOffset(this->length - 1, 0);

    this->length--;

    this->updateZeroIndex();
}

void UnsignedWholeNumberZero::removeFirstDigit()
{
    if(this->length == 0)
        return;

    for(size_t i = 0; i < this->getLength() - 1; i++)
        this->setDigitAtOffset(i, this->getDigitAtOffset(i + 1));
    this->setDigitAtOffset(this->getLength() - 1, 0);

    this->length--;

    this->updateZeroIndex();
}

UnsignedWholeNumberZero& UnsignedWholeNumberZero::operator=(const std::string& num)
{
    this->length = 0;
    this->startZeros = 0;
    this->numbers.clear();

    for(auto it = num.begin(); it != num.end(); it++) {
        this->setDigitAtOffset(
            std::distance(it, num.end() - 1),
            convertCharToNumber(*it)
        );
    }

    return (*this);
}

UnsignedWholeNumberZero& UnsignedWholeNumberZero::operator=(UnsignedWholeNumberZero const& num)
{
    this->length = num.getLength();
    this->numbers.clear();

    this->numbers.assign(
        num.numbers.begin(),
        num.numbers.end()
    );

    this->startZeros = num.startZeros;

    return (*this);
}

UnsignedWholeNumberZero UnsignedWholeNumberZero::operator+(UnsignedWholeNumberZero const& num) const
{
    UnsignedWholeNumberZero result;

    auto tlength = this->getLength();
    auto olength = num.getLength();
    auto mlength = max(tlength, olength);

    bool reminder = false;
    for(size_t i = 0; i < mlength; i++) {
        uint8_t tdigit = this->getDigitAtOffset(i);
        uint8_t odigit = num.getDigitAtOffset(i);

        uint8_t rdigit = tdigit + odigit;

        if(reminder)
            rdigit++;

        if(rdigit > 9) {
            reminder = true;
            rdigit -= 10;
        } else reminder = false;

        result.setDigitAtOffset(i, rdigit);
    }

    if(reminder)
        result.setDigitAtOffset(mlength, 1);

    return result;
}

UnsignedWholeNumberZero UnsignedWholeNumberZero::operator-(UnsignedWholeNumberZero const& num) const
{
    //if(num > (*this))
    //    throw OverflowException::create<UnsignedWholeNumberZero>(*this, num);

    UnsignedWholeNumberZero result;

    auto tlength = this->getLength();
    auto olength = num.getLength();

    bool reminder = false;
    for(size_t i = 0; i < tlength; i++) {
        uint8_t tdigit = this->getDigitAtOffset(i);
        uint8_t odigit = num.getDigitAtOffset(i);

        if(reminder) {
            tdigit--;
        }

        if(tdigit == 255) {
            tdigit = 9;
            reminder = true;
        } else if(odigit > tdigit) {
            reminder = true;
            tdigit += 10;
        } else reminder = false;

        uint8_t rdigit = tdigit - odigit;

        //if(rdigit != 0)
        result.setDigitAtOffset(i, rdigit);
    }

    return result;
}

UnsignedWholeNumberZero UnsignedWholeNumberZero::operator*(UnsignedWholeNumberZero const& num) const
{
    if(num.isZero() || this->isZero())
        return UnsignedWholeNumberZero("0");

    size_t tlen = this->getLength();
    size_t olen = num.getLength();

    std::vector<int> result(tlen + olen, 0);
    size_t i_n1 = 0, i_n2 = 0;
    for(size_t i = 0; i < tlen; i++) {
        size_t carry = 0;

        uint8_t tdigit = this->getDigitAtOffset(i);

        i_n2 = 0;

        for(size_t j = 0; j < olen; j++) {
            uint8_t odigit = num.getDigitAtOffset(j);

            uint64_t sum = tdigit * odigit + result[i_n1 + i_n2] + carry;

            carry = sum / 10;

            result[i_n1 + i_n2] = sum % 10;

            i_n2++;
        }

        if(carry > 0)
            result[i_n1 + i_n2] += carry;

        i_n1++;
    }

    int i = result.size() - 1;
    while(i >= 0 && result[i] == 0)
        i--;

    if(i == -1)
        return UnsignedWholeNumberZero("0");

    std::string s = "";

    while(i >= 0)
        s += std::to_string(result[i--]);

    auto result1 = UnsignedWholeNumberZero(s);
    result1.startZeros = this->startZeros + num.startZeros;
    result1.length += result1.startZeros;

    return result1;
}

UnsignedWholeNumberZero UnsignedWholeNumberZero::operator/(UnsignedWholeNumberZero const& num) const
{
    if(num.isZero())
        throw std::runtime_error("UnsignedWholeNumberZero is not divisible by 0");

    UnsignedWholeNumberZero result, t;
    result = "0";
    t = (*this);

    while(t >= num) {
        t -= num;
        result++;
    }

    return result;
}

UnsignedWholeNumberZero UnsignedWholeNumberZero::operator%(UnsignedWholeNumberZero const& num) const
{
    UnsignedWholeNumberZero result, t;
    t = (*this);

    while(t >= num)
        t -= num;

    return t;
}

UnsignedWholeNumberZero& UnsignedWholeNumberZero::operator+=(const UnsignedWholeNumberZero& num)
{
    (*this) = (*this) + num;

    return (*this);
}

UnsignedWholeNumberZero& UnsignedWholeNumberZero::operator-=(const UnsignedWholeNumberZero& num)
{
    (*this) = (*this) - num;

    return (*this);
}

UnsignedWholeNumberZero& UnsignedWholeNumberZero::operator*=(const UnsignedWholeNumberZero& num)
{
    (*this) = (*this) * num;

    return (*this);
}

UnsignedWholeNumberZero& UnsignedWholeNumberZero::operator/=(const UnsignedWholeNumberZero& num)
{
    (*this) = (*this) / num;

    return (*this);
}

bool UnsignedWholeNumberZero::operator==(UnsignedWholeNumberZero const& s) const
{
    return this->toString() == s.toString();
}

bool UnsignedWholeNumberZero::operator!=(UnsignedWholeNumberZero const& s) const
{
    return !this->operator==(s);
}

bool UnsignedWholeNumberZero::operator>(UnsignedWholeNumberZero const& num) const
{
    auto tlen = this->getLength();

    if(tlen == 0)
        return false;

    auto olen = num.getLength();

    if(olen == 0)
        return true;

    auto mlen = min(tlen, olen);

    for(size_t i = 1; i <= mlen; i++) {
        auto tdigit = this->getDigitAtOffset(tlen - i);
        auto odigit = num.getDigitAtOffset(olen - i);

        if(tdigit > odigit)
            return true;

        if(tdigit < odigit)
            return false;
    }

    return false;
}

bool UnsignedWholeNumberZero::operator<(UnsignedWholeNumberZero const& num) const
{
    auto tlen = this->getLength();
    auto olen = num.getLength();

    if(tlen == 0 && olen != 0)
        return true;

    if(olen == 0)
        return false;

    auto mlen = min(tlen, olen);

    for(size_t i = 1; i <= mlen; i++) {
        auto tdigit = this->getDigitAtOffset(tlen - i);
        auto odigit = num.getDigitAtOffset(olen - i);

        if(tdigit > odigit)
            return false;

        if(tdigit < odigit)
            return true;
    }

    return false;
}

bool UnsignedWholeNumberZero::operator>=(UnsignedWholeNumberZero const& num) const
{
    return !this->operator<(num);
}

bool UnsignedWholeNumberZero::operator<=(UnsignedWholeNumberZero const& num) const
{
    return !this->operator>(num);
}

void UnsignedWholeNumberZero::setDigitAtOffset(size_t offset, uint8_t digit)
{
    UnsignedWholeNumber::setDigitAtOffset(offset, digit);

    this->updateZeroIndex();
}

uint8_t UnsignedWholeNumberZero::getDigitAtOffset(size_t offset) const
{
    return UnsignedWholeNumber::getDigitAtOffset(offset);
}

std::string UnsignedWholeNumberZero::toString() const
{
    std::stringstream sstream;

    if(this->getLength() == 0)
        sstream << 0;

    else {
        for(auto i = this->getLength(); i-- > 0;)
            sstream << (int) this->getDigitAtOffset(i);
    }

    return sstream.str();
}

bool WholeNumber::isStringInstanceValid(std::string const& number)
{
    return (
               UnsignedWholeNumber::isStringInstanceValid(number)
               || std::regex_match(number, std::regex("-[0-9]{1,}"))
           ) && (
               !std::regex_match(number, std::regex("-[0]{1,}"))
           );
}

UnsignedWholeNumber Number::convertToWholeNumber() const
{
    UnsignedWholeNumber result;

    size_t offset = 0;

    if(!this->isWhole()) {
        for(size_t i = 0; i < this->floatingPoint.getLength(); i++) {
            result.setDigitAtOffset(i, this->floatingPoint.getDigitAtOffset(i));

            if(i == (this->floatingPoint.getLength() - 1)) {
                offset = i + 1;
                break;
            }
        }
    }


    for(size_t i = 0; i < this->integer.getLength(); i++)
        result.setDigitAtOffset(
            i + offset,
            this->integer.getDigitAtOffset(i)
        );

    return result;
}

Number::Number()
{

}

Number::Number(std::string const& num)
{
    this->operator=(num);
}

Number::Number(double num)
{
    std::string dstr = std::to_string(num);

    if(std::find(dstr.begin(), dstr.end(), '.') != dstr.end()) {
        dstr.erase(dstr.find_last_not_of('0') + 1, std::string::npos);
    }

    if(dstr[dstr.size() - 1] == '.')
        dstr.erase(dstr.begin() + (dstr.size() - 1));

    this->operator=(dstr);
}

Number::~Number()
{

}

bool Number::isStringInstanceValid(std::string const& num)
{
    return (
               WholeNumber::isStringInstanceValid(num)
               || std::regex_match(num, std::regex("[0-9]{1,}\\.[0-9]{1,}"))
               || std::regex_match(num, std::regex("-[0-9]{1,}\\.[0-9]{1,}"))
           ) && (
               !std::regex_match(num, std::regex("-[0]{1,}\\.[0]{2,}"))
               && !std::regex_match(num, std::regex("-[0-9]{1,}\\.[0]{2,}"))
               && !std::regex_match(num, std::regex("[0-9]{1,}\\.[0]{2,}"))
           );
}

Number const& Number::operator=(std::string const& num)
{
    if(!Number::isStringInstanceValid(num))
        throw InvalidNumberException();

    //Since here we have string copies.
    //We can add negative support from here (And remove - from the string).
    //Optimizing this vector to pointers + len would be possibility.
    //But this would be hard to read then.
    auto numbers = splitString(num, '.');
    if(numbers.at(0).at(0) == '-') {
        this->positive = false;
        numbers.at(0) = numbers.at(0).substr(1);
    } else this->positive = true;

    //Lets continue with the handling of the number.
    this->integer = numbers.at(0);

    if(numbers.size() == 2) {
        this->floatingPoint = numbers.at(1);

        while(
            this->floatingPoint.getLength() != 0
            && this->floatingPoint.getDigitAtOffset(0) == 0
            ) {
            this->floatingPoint.removeFirstDigit();
        }
    } else
        this->floatingPoint = "0";

    return (*this);
}

Number Number::operator+(std::string const& num) const
{
    Number other = Number(num);

    return this->operator+(other);
}

Number Number::operator+(Number const& num) const
{
    Number number;

    if(this->isNegative() && num.isNegative())
        number.positive = false;

    else if(!this->isNegative() && !num.isNegative())
        number.positive = true;

    else if(this->isNegative())
        return num.operator-(this->absolute());

    else if(num.isNegative())
        return this->operator-(num.absolute());

    UnsignedWholeNumberZero tfpoint = this->getFloatingPoint();
    UnsignedWholeNumberZero ofpoint = num.getFloatingPoint();

    auto tflen = tfpoint.getLength();
    auto oflen = ofpoint.getLength();
    auto mlen = max(tflen, oflen);

    if(tflen < oflen) {
        tfpoint *= Number::calcPower(10, oflen - tflen).getInteger().toString();
    } else if(oflen < tflen) {
        ofpoint *= Number::calcPower(10, tflen - oflen).getInteger().toString();
    }

    number.floatingPoint = tfpoint + ofpoint;

    bool reminder = false;
    if(mlen < number.getFloatingPoint().getLength()) {
        number.floatingPoint.removeLastDigit();
        reminder = true;
    }

    number.integer = this->getInteger() + num.getInteger();

    if(reminder)
        number.integer++;

    return number;
}

Number Number::operator-(std::string const& num) const
{
    Number o = Number(num);

    return this->operator-(o);
}

Number Number::operator-(Number const& num) const
{
    if(num.isNegative())
        return this->operator+(num.absolute());

    Number number;

    if(this->isNegative()) {
        auto mlen = max(
            this->getFloatingPoint().getLength(),
            num.getFloatingPoint().getLength()
        );

        number.floatingPoint = this->getFloatingPoint() + num.getFloatingPoint();
        number.integer = this->getInteger() + num.getInteger();

        if(number.getFloatingPoint().getLength() > mlen) {
            number.floatingPoint.removeLastDigit();
            number.integer++;
        }

        number.positive = false;

        return number;
    }

    if(this->getInteger() < num.getInteger()) {
        number.integer = num.getInteger() - this->getInteger();
        number.positive = false;
    } else {
        number.integer = this->getInteger() - num.getInteger();
        number.positive = true;
    }

    UnsignedWholeNumberZero tfpoint = this->getFloatingPoint();
    UnsignedWholeNumberZero ofpoint = num.getFloatingPoint();

    if(tfpoint.isZero() && ofpoint.isZero())
        return number;

    if(ofpoint.isZero()) {
        number.floatingPoint = tfpoint;
        return number;
    }

    auto tflen = tfpoint.getLength();
    auto oflen = ofpoint.getLength();

    if(tflen < oflen) {
        tfpoint *= Number::calcPower(10, oflen - tflen).getInteger().toString();
    } else if(oflen < tflen) {
        ofpoint *= Number::calcPower(10, tflen - oflen).getInteger().toString();
    }

    bool oneTooMuch = false;
    bool flip = false;

    if(tfpoint.isZero() || ofpoint > tfpoint) {
        oflen = ofpoint.getLength();

        if(number >= Number("1"))
            number.integer--;

        else if(number.isZero()) {
            number = Number("-0.0");
            flip = true;
        } else
            number.integer++;

        tfpoint += Number::calcPower(10, oflen).getInteger().toString();

        oneTooMuch = true;
    }

    number.floatingPoint = tfpoint - ofpoint;

    if(oneTooMuch) {
        number.floatingPoint.removeLastDigit();

        if(flip) {
            number.floatingPoint = (UnsignedWholeNumberZero(
                Number::calcPower(10, number.floatingPoint.getLength()).getInteger()
                                                                       .toString()) - number.floatingPoint);
            number.floatingPoint.removeLastDigit();
        }
    }

    return number;
}

Number Number::operator*(std::string const& num) const
{
    Number number = Number(num);

    return this->operator*(number);
}

Number Number::operator*(Number const& num) const
{
    Number result;
    result.positive = (this->isNegative() == num.isNegative()) || this->isZero() || num.isZero();

    UnsignedWholeNumber tnum = this->convertToWholeNumber();
    UnsignedWholeNumber onum = num.convertToWholeNumber();
    UnsignedWholeNumber rnum = tnum * onum;

    size_t floatingPointCount = this->getFloatingPoint().getLength() + num.getFloatingPoint().getLength();

    for(size_t i = 0; i < rnum.getLength(); i++) {
        if(i >= floatingPointCount) {
            size_t offset = i - floatingPointCount;
            result.integer.setDigitAtOffset(offset, rnum.getDigitAtOffset(i));
        } else {
            result.floatingPoint.setDigitAtOffset(i, rnum.getDigitAtOffset(i));
        }
    }

    if(floatingPointCount > rnum.getLength()) {
        auto diff = floatingPointCount - rnum.getLength();

        for(size_t i = 0; i < diff; i++)
            result.floatingPoint.setDigitAtOffset(i + rnum.getLength(), 0);
    }

    return result;
}

size_t Number::dividePrecision = 51;

Number Number::operator/(std::string const& num) const
{
    Number number = Number(num);

    return this->operator/(number);
}

Number Number::operator%(Number const& num) const
{
    Number t;
    t = (*this);

    while(t >= num) {
        t -= num;
    }

    return t;
}

Number Number::operator%(std::string const& num) const
{
    Number number = Number(num);

    return this->operator%(number);
}

bool startsWith(std::string const& str, std::string const& needle)
{
    if(str.size() < needle.size())
        return false;

    for(size_t i = 0; i < needle.size(); i++) {
        if(str[i] != needle[i])
            return false;
    }

    return true;
}

bool endsWith(std::string const& str, std::string const& needle)
{
    if(str.size() < needle.size())
        return false;

    for(size_t i = 0; i < needle.size(); i++) {
        size_t strIdx = str.size() - needle.size() + i;

        if(str[strIdx] != needle[i])
            return false;
    }

    return true;
}

bool containsOnly(std::string const& str, std::string const& needle)
{
    for(auto num : str) {
        bool suc = false;

        for(auto _num : needle)
            if(num == _num) {
                suc = true;
                break;
            }

        if(!suc)
            return false;
    }

    return true;
}

void divide(UnsignedWholeNumber const& tnum, UnsignedWholeNumber const& onum, Number& result)
{
    if(onum.toString() == "1") {
        result = Number(tnum.toString());
        return;
    }

    if(onum.toString() == "-1") {
        result = Number(tnum.toString()) * "-1";
        return;
    }

    auto tlen = tnum.getLength();

    size_t idx = 0;
    UnsignedWholeNumber temp = std::to_string((int) tnum.getDigitAtOffset(tlen - idx - 1));
    while(temp < onum && tlen - idx != 1) {
        temp *= UnsignedWholeNumber("10");
        temp += std::to_string((int) tnum.getDigitAtOffset((tlen - (++idx) - 1)));
    }

    std::vector<int> resInt;
    std::vector<int> resFloat;

    bool _int = true;
    while(true) {
        if(_int)
            resInt.push_back(std::atoi((temp / onum).toString().c_str()));
        else
            resFloat.push_back(std::atoi((temp / onum).toString().c_str()));

        if(tlen > idx + 1) {
            temp = (temp % onum) * UnsignedWholeNumber("10");
            temp +=
                UnsignedWholeNumber(
                    \
                    std::to_string(
                        (int) tnum.getDigitAtOffset(tlen - (++idx) - 1)
                    )
                );
        } else if(temp.toString() == "0")
            break;

        else if((tlen + Number::getDividePrecision()) > (idx + 1)) {
            _int = false;
            temp = (temp % onum) * UnsignedWholeNumber("10");
            idx++;
        } else break;
    }

    // If divisor is greater than number 
    if(resInt.size() == 0 && resFloat.size() == 0)
        return;

    std::stringstream ss;
    if(result.isNegative())
        ss << "-";

    for(auto it = resInt.begin(); it != resInt.end(); it++)
        ss << *it;

    if(!resFloat.empty()) {
        ss << ".";

        for(auto it = resFloat.begin(); it != resFloat.end(); it++)
            ss << *it;
    }

    std::string _result = ss.str();

    if(endsWith(_result, ".0"))
        _result = _result.substr(0, _result.size() - 2);

    if(containsOnly(_result, "0.-"))
        _result = "0";

    //auto it = std::find(_result.begin(), _result.end(), '.');
    //if(it != _result.end()) {
//
    //}

    if(startsWith(_result, "-0"))
        _result = _result.substr(1, _result.size() - 1);

    result = _result;
}

Number Number::operator/(Number const& num) const
{
    if(num.isZero())
        throw std::runtime_error("Number is not divisible by 0!");

    Number result;
    result.positive = (this->isNegative() == num.isNegative()) || this->isZero() || num.isZero();

    auto tlen = this->getFloatingPoint().getLength();
    auto olen = num.getFloatingPoint().getLength();
    auto minlen = min(tlen, olen);
    auto maxlen = max(tlen, olen);

    UnsignedWholeNumber tnum = this->convertToWholeNumber();
    UnsignedWholeNumber onum = num.convertToWholeNumber();

    if(tlen > olen)
        onum *= Number::calcPower(10, maxlen - minlen).getInteger();

    else if(olen > tlen)
        tnum *= Number::calcPower(10, maxlen - minlen).getInteger();

    divide(tnum, onum, result);

    return result;
}

Number& Number::operator+=(Number const& num)
{
    (*this) = (*this) + num;

    return (*this);
}

Number& Number::operator+=(std::string const& num)
{
    Number number(num);

    return this->operator+=(number);
}

Number& Number::operator-=(Number const& num)
{
    (*this) = (*this) - num;

    return (*this);
}

Number& Number::operator-=(std::string const& num)
{
    Number number(num);

    return this->operator-=(number);
}


Number& Number::operator*=(Number const& num)
{
    (*this) = (*this) * num;

    return (*this);
}

Number& Number::operator*=(std::string const& num)
{
    Number number(num);

    return this->operator*=(number);
}


Number& Number::operator/=(Number const& num)
{
    (*this) = (*this) / num;

    return (*this);
}

Number& Number::operator/=(std::string const& num)
{
    Number number(num);

    return this->operator/=(number);
}


Number& Number::operator++()
{
    Number add;
    add = "1";

    (*this) += add;

    return (*this);
}

Number& Number::operator--()
{
    Number min;
    min = "1";

    (*this) -= min;

    return (*this);
}

Number& Number::operator++(int)
{
    Number add;
    add = "1";

    (*this) += add;

    return (*this);
}

Number& Number::operator--(int)
{
    Number min;
    min = "1";

    (*this) -= min;

    return (*this);
}

bool Number::operator==(Number const& num) const
{
    return this->toString() == num.toString();
}

bool Number::operator!=(Number const& num) const
{
    return !this->operator==(num);
}

bool Number::operator>(Number const& num) const
{
    auto tnegative = this->isNegative();
    auto onegative = num.isNegative();

    if(!tnegative && onegative)
        return true;

    if(tnegative && !onegative)
        return false;

    bool invert = false;
    if(tnegative) invert = true;

    bool result = this->integer > num.getInteger();

    if(result || this->integer != num.getInteger())
        return invert ? !result : result;

    result = this->floatingPoint > num.getFloatingPoint();

    return invert ? !result : result;
}

bool Number::operator<(Number const& num) const
{
    auto tnegative = this->isNegative();
    auto onegative = num.isNegative();

    if(!tnegative && onegative)
        return false;

    if(tnegative && !onegative)
        return true;

    bool invert = false;
    if(tnegative) invert = true;

    bool result = this->integer < num.getInteger();

    if(result || this->integer != num.getInteger())
        return invert ? !result : result;

    result = this->floatingPoint < num.getFloatingPoint();

    return invert ? !result : result;
}

bool Number::operator>=(Number const& num) const
{
    return !this->operator<(num);
}

bool Number::operator<=(Number const& num) const
{
    return !this->operator>(num);
}

std::ostream& operator<<(std::ostream& os, Number const& num)
{
    os << num.toString();

    return os;
}

Number Number::absolute() const
{
    Number result = (*this);
    result.positive = true;

    return result;
}

Number Number::inverse() const
{
    Number result, x;
    Number temp = (*this);

    do {
        x = temp;
        result = x * (Number("2") - ((*this) * x));
        temp = result;
    } while((result - x).absolute() > Number("0.001"));

    return result;
}

UnsignedWholeNumber const& Number::getInteger() const
{
    return this->integer;
}

UnsignedWholeNumberZero const& Number::getFloatingPoint() const
{
    return this->floatingPoint;
}

std::string Number::toString() const
{
    std::stringstream sstream;

    if(this->isNegative())
        sstream << "-";

    sstream << this->integer.toString() << '.' << this->floatingPoint.toString();

    return sstream.str();
}

bool Number::isWhole() const
{
    return this->floatingPoint.isZero();
}

bool Number::isPositive() const
{
    return this->positive && !this->isZero();
}

bool Number::isNegative() const
{
    return !this->positive;
}

bool Number::isZero() const
{
    return this->integer.isZero() && this->floatingPoint.isZero();
}

/**
 * MATH FUNCTIONS
 */
Number Number::calcFactorial(int n)
{
    Number result("1");

    for(int i = 0; i < n; i++)
        result *= Number(std::to_string(i + 1));

    return result;
}

Number Number::findNextPrime(Number n)
{
    do {
        n++;
    } while((!n.isPrime()));

    return n;
}

Number Number::calcPower(double d, int p)
{
    int pabs = std::abs(p);

    if(pabs == 0)
        return Number("1");

    Number number = Number(d);
    Number result = number;

    for(int i = 1; i < pabs; i++)
        result *= number;

    if(p < 0)
        result = Number("1") / result;

    return result;
}

Number Number::calcPower(int n) const
{
    int nabs = std::abs(n);

    if(n == 0)
        return Number("1");

    Number number = *this;
    Number result = number;

    for(int i = 1; i < nabs; i++)
        result *= number;

    if(n < 0)
        result = Number("1") / result;

    return result;
}

#define MATH_PI_025 0.785375
#define MATH_PI_05 1.57075
#define MATH_PI 3.1415
#define MATH_PI_15 4.71225
#define MATH_PI_2 6.283

Number Number::calcSin(double d)
{
    if(d == 0.)
        return Number("0");

    if(d < 0)
        d = abs(d) + MATH_PI;

    while(d > MATH_PI_2)
        d -= MATH_PI_2;

    //We map sin to first quadrant.
    bool minAttached = false;

    if(d > MATH_PI_15) {
        d = MATH_PI_2 - d;
        minAttached = true;
    } else if(d > MATH_PI) {
        d = d - MATH_PI;
        minAttached = true;
    } else if(d > MATH_PI_05)
        d = d - 2 * (d - MATH_PI_05);

    //Its accurate only to 45 deg
    if(d > MATH_PI_025)
        return Number::calcCos(MATH_PI_05 - d);

    Number sin(d);
    Number result = sin;

    size_t it = 1;
    while(true) {
        Number oldresult = result;

        result += Number::calcPower(-1, it - 1) * ((sin.calcPower((it * 2) + 1) / Number::calcFactorial((it * 2) + 1)));

        if(oldresult == result)
            break;

        if(it == 100)
            break;

        it++;
    }

    if(minAttached)
        result.positive = false;

    return result;
}

Number Number::calcCos(double d)
{
    d = abs(d);

    while(d > MATH_PI_2)
        d -= MATH_PI_2;

    if(d == 0.)
        return Number("1");

    //We map sin to first quadrant.
    bool minAttached = false;

    if(d > MATH_PI_15)
        d = MATH_PI_2 - d;

    else if(d > MATH_PI) {
        d = d - MATH_PI;
        minAttached = true;
    } else if(d > MATH_PI_05) {
        d = d - 2 * (d - MATH_PI_05);
        minAttached = true;
    }

    //It's accurate only to 45 deg
    if(d > MATH_PI_025)
        return Number::calcSin(MATH_PI_05 - d);

    Number sin(d);
    Number result = Number("1");

    size_t it = 1;
    while(true) {
        Number oldresult = result;

        result += Number::calcPower(-1, it) * ((sin.calcPower(it * 2) / Number::calcFactorial(it * 2)));

        if(oldresult == result)
            break;

        if(it == 100)
            break;

        it++;
    }

    if(minAttached)
        result.positive = false;

    return result;
}

Number Number::calcSqrt(double d)
{
    return Number::calcSqrt(Number(d));
}

Number Number::calcSqrt(const Number& num)
{
    if(num < Number("0"))
        throw OverflowException::create<Number>(Number("0"), num);

    if(num == Number("0"))
        return Number("0.0");

    Number s = num;
    Number y("1");

    while((s - y).absolute() > Number::getSmallestNumber()) {
        s = (s + y) / "2";
        y = num / s;
    }

    return s;
}

Number Number::calcLogN(double d)
{
    return Number::calcLogN(Number(d));
}

Number Number::calcLogN(const Number& num)
{
    if(num <= Number("0"))
        throw OverflowException::create<Number>(Number("0"), num);

    if(num == Number("1"))
        return Number("0");

    if(num < Number("1"))
        return Number::calcLogN(Number("1") / num) * Number("-1");

    if(num > Number("1.01"))
        return Number("2") * Number::calcLogN(Number::calcSqrt(num));

    Number _num = num - "1";
    Number num2(_num);
    Number sum(_num);
    Number old_sum;
    size_t it = 2;
    do {
        old_sum = sum;
        num2 *= _num;

        if((it % 2) == 0)
            sum -= (num2 / Number(std::to_string(it)));
        else
            sum += (num2 / Number(std::to_string(it)));

        it++;
    } while((old_sum - sum).absolute() > Number::getSmallestNumber());

    return sum;
}

Number Number::calcPI(int digits)
{
    if(digits == 0)
        return Number("3");

    if(digits == 1)
        return Number("3.1");

    auto oldPrecision = Number::getDividePrecision();
    Number::setDividePrecision(digits * 2);

    Number result("0");

    int it = 0;
    while(true) {
        Number add = (
                         Number::calcFactorial(it * 6) *
                         ((Number("545140134") * Number(std::to_string(it))) + Number("13591409"))
                     ) / (
                         Number::calcFactorial(it * 3) * Number::calcFactorial(it).calcPower(3) *
                         Number("-262537412640768000").calcPower(it)
                     );

        if(add == Number("0"))
            break;

        result += add;

        it++;
    }

    result /= "426880";
    result /= Number::calcSqrt(Number("10005"));

    Number::setDividePrecision(digits);

    result = Number("1") / result;

    Number::setDividePrecision(oldPrecision);

    return result;
}

Number Number::calcAverage(Number *data, int n) {
    Number result("0");

    for(int i = 0; i < n; i++)
        result += data[i];

    return result / Number(std::to_string(n));
}

int Number::find(Number *data, int n, Number const& num) {
    for(int i = 0; i < n; i++) {
        if(data[i] == num)
            return i;
    }

    throw std::invalid_argument("Number::find(data, n, num) was not able to find num in data array!");
}

template <typename T>
void swapc(T* a, T* b) {
    T temp = *a;
    *a = *b;
    *b = temp;
}

void Number::sort(Number *data, int n) {
    if(n == 0)
        return;

    for(int i = 0; i < n - 1; i++)
    {
        bool swapped = false;

        for(int j = 0; j < n - i - 1; j++)
        {

            if(data[j] > data[j+1]) {
                swapc(&(data[j]), &(data[j + 1]));
                swapped = true;
            }

        }

        if(!swapped)
            break;
    }
}

const Number& Number::getSmallestNumber()
{
    static bool calculated = false;
    static Number num;

    if(!calculated) {
        num = Number("0." + std::string(Number::dividePrecision - 2, '0') + '1');

        calculated = true;
    }

    return num;
}

bool Number::isPrime() const
{
    if(!this->isWhole())
        return false;

    if(!this->isPositive())
        return false;

    if((*this) == std::string("2") || (*this) == std::string("3"))
        return true;

    if(((*this) / "2").isWhole() || ((*this) / "3").isWhole())
        return false;

    auto divisor = Number("6");
    while((divisor * divisor) - (divisor * std::string("2")) + std::string("1") <= (*this)) {

        if(((*this) / (divisor - "1")).isWhole())
            return false;

        if(((*this) / (divisor + "1")).isWhole())
            return false;

        divisor += "6";
    }

    return true;
}